# Configuracion para i3wm

    He tomado como referencia el trabajo de [GerrySoft](https://github.com/GerrySoft) para la elavoracion de mis configuraciones.

### Algunos comandos personales

`$mod+u` 	subir volumen

`$mod+i`	bajar volumen

`$mod+Shift+u`	subir brillo

`$mod+Shift+i`	bajar brillo

#### paquetes necesarios para esta configuracion

`pacman -S dmenu feh picom brightnessctl py3status` 
